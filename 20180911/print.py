import string

def draw_square(num:int):
    # output your answer here using print
    pass

# Don't touch this
if __name__ == "__main__":
    while 1:
        inp = input()
        try:
            draw_square( int( inp ) )
            break
        except ValueError:
            print( "It is not an integer".format( inp ) )
