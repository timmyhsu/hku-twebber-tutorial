## Introduction

**【Sem1】**

**Week 1** Greeting, Seminar Syllabus, Coding& Internship Sharing, Python installation

**Week 2** Basic Coding Logic& Syntax，html & CSS

**Week 3** [Web] Webpage practice -> TSA Webpage

**Week 4** [Data Processing] Introduction to Python

**Week 5** [Data Processing] Web Crawler with Python

**Week 6** [Web] Webpage practice, Intro to frontend framework e.x Meteor/React.Js

**Week 7** [Web] Webpage practice –> rework TSA Homepage with the framework

**Week 8** [Web] Webpage practice -> End of Sem1

**【Sem2】**

[Web] Computer Networking, Backend, Project Based

[FinTech] Justin –> Intro to Block Chain &How to build your private Block Chain

[Data Processing] Benny -> Data Processing using numpy, pandas in Python

## Schedule Discussion


## Preparation

**1. Install miniAnaconda**

https://conda.io/miniconda.html

**2. Register a gitlab account**

https://gitlab.com/users/sign_in

**3. Install Git**

https://git-scm.com/downloads

**4. Install IDE/Text Editor**

Sublime

https://www.sublimetext.com/

Notepad++

https://notepad-plus-plus.org/zh/


**5. Read about Python official tutorial**

https://docs.python.org/3/tutorial/introduction.html


## Basic Coding Practice

**1. Swap**

**2. Prime Number**