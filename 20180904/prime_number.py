def get_prime_number(num:int):
	prime_number=0 
    for i in range (2,num): #要檢驗的數字們(<輸入值)
		for j in range (2,i): #不能除到質數的因數(1和自己)
			non_devisible=True 
			if i%j==0: #沒有整除的繼續迴圈，有一個整除就false，跳到下個i去算
				non_devisible=False
				break
		if non_devisible: #會使用到代表值是true
			prime_number+=1 #標記此i循環中多了一個質數
	return prime_number #最後所有迴圈跑完回傳小於輸入值之質數個數

#DO NOT TOUCH THE MAIN FUNCTION
if __name__ == '__main__':
    print(get_prime_number(int(input())))